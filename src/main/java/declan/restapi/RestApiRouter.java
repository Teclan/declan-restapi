package declan.restapi;

import org.restlet.routing.Router;

public interface RestApiRouter {
    public void createRoutes(Router router);

}
